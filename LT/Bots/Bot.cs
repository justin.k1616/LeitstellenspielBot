﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LT.Bots
{
    public class Bot
    {
        internal Bot()
        {
        }

        public Task Execute(Settings settings)
        {
            return Run(settings);
        }

        protected virtual async Task Run(Settings settings)
        {
            await Task.Delay(1000);
            throw new NotImplementedException("Please override BotBase::Run().");
        }

    }
}
