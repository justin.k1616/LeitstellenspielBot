﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using LT.Helper;
using LT.Models;

namespace LT
{
  public class Settings
  {
    /// <summary>
    /// Contains object instance of <see cref="LT.Models.User"/>.
    /// </summary>
    public Models.User User { get; set; } = new Models.User();

    /// <summary>
    /// Contains object instance of <see cref="LT.Helper.HTTP"/>.
    /// </summary>
    public Helper.HTTP Client { get; set; } = new Helper.HTTP();
  }
}
